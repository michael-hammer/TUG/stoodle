<?php
  // ------------------------------------------------------------------
  // This is free software; you can redistribute it and/or modify
  // it under the terms of the GNU General Public License as published by
  // the Free Software Foundation; either version 2 of the License, or
  // (at your option) any later version.
  // 
  // This program is distributed in the hope that it will be useful,
  // but WITHOUT ANY WARRANTY; without even the implied warranty of
  // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  // GNU General Public License for more details.
  // 
  // You should have received a copy of the GNU General Public License
  // along with this program; if not, write to the Free Software
  // Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
  // 
  // Authors: Michael Hammer
  // ------------------------------------------------------------------*/

include("htdocs/header.html");
include_once("src/common.php");
include_once("src/database_handler.php");
include_once("src/student_importer.php");

$db = new DatabaseHandler('db/database.db');

$temp_path = 'uploads/data.cvs';

createHeaderMenu();

switch( $_GET['mode'] ) {
case 'import':
  print "<h1>Import</h1>";
  if( $_GET['target'] == 'student' ) {
    print "<h2>Student</h2>";
    $student_importer = new StudentImporter($db);
    if( $_FILES['uploadedfile']['name'] ) {
      move_uploaded_file($_FILES['uploadedfile']['tmp_name'],$temp_path);
      $student_importer -> importData($temp_path);
    }
    $student_importer -> createFileImporter();
    $student_importer -> manualCreate();
    createFooter();
  }
  print "<p>Sie müssen den Typ wählen: <a href=\"import_export.php?mode=import&target=student\">Student</a></p>";
  createFooter();
  break;
case 'export':
  print "<h1>Export</h1>";
  createFooter();
  break;
}

print "<p>Sie müssen zwischen <a href=\"import_export.php?mode=import&target=student\">Import</a> und Export wählen!</p>";
createFooter();

?>
