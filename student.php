<?php
  // ------------------------------------------------------------------
  // This is free software; you can redistribute it and/or modify
  // it under the terms of the GNU General Public License as published by
  // the Free Software Foundation; either version 2 of the License, or
  // (at your option) any later version.
  // 
  // This program is distributed in the hope that it will be useful,
  // but WITHOUT ANY WARRANTY; without even the implied warranty of
  // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  // GNU General Public License for more details.
  // 
  // You should have received a copy of the GNU General Public License
  // along with this program; if not, write to the Free Software
  // Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
  // 
  // Authors: Michael Hammer
  // ------------------------------------------------------------------*/

include_once("src/database_handler.php");
include_once("src/ldap_handler.php");
include_once("src/student_card_handler.php");
include_once("src/learngroup_card_handler.php");
include_once("src/student_selector.php");
include_once("src/common.php");

$db = new DatabaseHandler('db/database.db');
$ldap = new LDAPHandler();

include("htdocs/header.html");
createHeaderMenu();

switch( $_GET['mode'] ) {
 case 'form':
   if( $_GET['matnr'] ) {
     $student_card = new StudentCardHandler($db,$ldap,$_GET['matnr']);
     $student_card -> createCard();
     createFooter();
   }
   if( $_GET['groupname'] ) {
     $learngroup_card = new LearngroupCardHandler($db,$ldap,$_GET['groupname']);
     $learngroup_card -> createCard();
     createFooter();
   }
   if( $_GET['learngroups'] ) {
     $student_selector = new StudentSelector($db);
     $student_selector -> createLearngroupsSelection();     
     createFooter();
   }
   if( $_GET['search'] ) {
     $search_str = $_POST['search'];
     $config_search = array( 'matnr' => $_POST['matnr'],
			     'last_name' => $_POST['last_name'],
			     'first_name' => $_POST['first_name'],
			     'learngroups' => $_POST['learngroups'] );

     $count = $db -> countMatchingStudents($search_str,$config_search);
     if( $count == 1 ) {
       $student_card = new StudentCardHandler($db,$ldap,$db->findSingleMatNr($search_str,$config_search));
       $student_card -> createCard();
     } elseif ( $count > 1 ) {
       $student_selector = new StudentSelector($db);
       $student_selector -> createSelection($search_str,$config_search);
     } else {
       print "<p>Student leider nicht gefunden! -> ".
	 "<a href=\"import_export.php?mode=import&type=student\">Erstelle</a> neuen Studenteneintrag<p>";
     }
     createFooter();
   }
   break;
 case 'edit_coaching':
   if( $_GET['matnr'] ) {
     $student_card = new StudentCardHandler($db,$ldap,$_GET['matnr']);
     $student_card -> createCoachingEdit($_GET['timestamp']);
     createFooter();
   }
   if( $_GET['groupname'] ) {
     $learngroup_card = new LearngroupCardHandler($db,$ldap,$_GET['groupname']);
     $learngroup_card -> createCoachingEdit($_GET['timestamp']);
     createFooter();
   }
   break;
}

?>
