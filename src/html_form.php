<?php
  // ------------------------------------------------------------------
  // This is free software; you can redistribute it and/or modify
  // it under the terms of the GNU General Public License as published by
  // the Free Software Foundation; either version 2 of the License, or
  // (at your option) any later version.
  // 
  // This program is distributed in the hope that it will be useful,
  // but WITHOUT ANY WARRANTY; without even the implied warranty of
  // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  // GNU General Public License for more details.
  // 
  // You should have received a copy of the GNU General Public License
  // along with this program; if not, write to the Free Software
  // Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
  // 
  // Authors: Michael Hammer
  // ------------------------------------------------------------------*/

function OTform($action = null,$method = null) {
  return("<form".( $action ? " action=\"".$action."\"" : "").( $method ? " method=\"".$method."\">" : ">\n"));
  }

function input($type, $name, $value = null, $size = null, $append = null) {
  return("<input type=\"".$type."\"".
	 " name=\"".$name."\"".
	 ( $value ? " value=\"".$value."\"" : '' ).
	 ( $size ? " size=\"".$size."\"" : '' ).
	 ( $append ? " $append" : '' ).
	 ">");
}

function CTform() {
  return( "</form>\n" );
}
		
?>