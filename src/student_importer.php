<?php
  // ------------------------------------------------------------------
  // This is free software; you can redistribute it and/or modify
  // it under the terms of the GNU General Public License as published by
  // the Free Software Foundation; either version 2 of the License, or
  // (at your option) any later version.
  // 
  // This program is distributed in the hope that it will be useful,
  // but WITHOUT ANY WARRANTY; without even the implied warranty of
  // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  // GNU General Public License for more details.
  // 
  // You should have received a copy of the GNU General Public License
  // along with this program; if not, write to the Free Software
  // Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
  // 
  // Authors: Michael Hammer
  // ------------------------------------------------------------------*/

class StudentImporter {
  protected $db_;
  
  //------------------------------------------------------------------
  function __construct($db) {
    $this->db_=$db;
  }

  //------------------------------------------------------------------
  function createFileImporter() {
    print "<h3>Automatischer Import aus TUGOnlnie</h3>".
      "<form enctype=\"multipart/form-data\" action=\"import_export.php?mode=import&target=student\" method=\"POST\">"."\n".
      "Choose a file to upload:"."\n".
      "<input name=\"uploadedfile\" type=\"file\" />"."\n".
      "<input type=\"submit\" value=\"Upload Datei\" />"."\n".
      "</form>";
  }

  //------------------------------------------------------------------
  function importData($temp_path) {
    $index = array( 'matnr'=>4 , 'last_name'=>2 , 'first_name'=>3 , 'email'=>9 );
    $handle = fopen ($temp_path,"r");
    $content = array();
    while( ($data = fgetcsv ($handle, 1000, ",")) !== FALSE ) {
      $row++;
      if( $row < 4 )
	continue;
      $content_row = array();
      foreach( $index as $column => $i ) {
	//	$content_row[$column] = iconv('ISO-8859-15','utf-8',$data[$i]);
	$content_row[$column] = $data[$i];
      }
      $this->db_->insertStudent($content_row);
    }
    fclose ($handle);
  }

  //------------------------------------------------------------------
  function manualCreate() {
    print "<h3>Manuelle Erstellung</h3><form action=\"writer.php?target=student&mode=new\" method=\"post\"><table>"."\n".
      "<tr><td>Matrikelnummer:</td><td><input name=\"matnr\" type=\"text\" size=\"50\"></td></tr>"."\n".
      "<tr><td>Familienname:</td><td><input name=\"last_name\" type=\"text\" size=\"50\"></td></tr>"."\n".
      "<tr><td>Vorname:</td><td><input name=\"first_name\" type=\"text\" size=\"50\"></td></tr>"."\n".
      "<tr><td>Emailadresse:</td><td><input name=\"email\" type=\"text\" size=\"50\"></td></tr>"."\n".
      "<tr><td><input type=\"submit\" value=\"Speichern\"></td><td align=\"right\"><input type=\"reset\" value=\"Cancel\"></td></tr>"."\n".
      "</table></form>";
  }

  }

?>