<?php
  // ------------------------------------------------------------------
  // This is free software; you can redistribute it and/or modify
  // it under the terms of the GNU General Public License as published by
  // the Free Software Foundation; either version 2 of the License, or
  // (at your option) any later version.
  // 
  // This program is distributed in the hope that it will be useful,
  // but WITHOUT ANY WARRANTY; without even the implied warranty of
  // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  // GNU General Public License for more details.
  // 
  // You should have received a copy of the GNU General Public License
  // along with this program; if not, write to the Free Software
  // Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
  // 
  // Authors: Michael Hammer
  // ------------------------------------------------------------------*/

include_once("card_handler.php");

class LearngroupCardHandler extends CardHandler {
  protected $groupname_;

  //------------------------------------------------------------------
  function __construct($db, $ldap, $groupname) {
    parent::__construct($db,$ldap);
    $this->groupname_=$groupname;
  }

  //------------------------------------------------------------------
  protected function createHeader() {
    $matnr_array = $this->db_->findLearngroupsMatnr($this->groupname_);
    print "<h1><a href=\"student.php?mode=form&groupname=".$this->groupname_."\">".$this->groupname_."</a></h1>\n";

    print "<table>\n".
      "  <tr><td><b>Gruppenmitglieder:</b></td><td></td><td><td>";
    deleteGroupLink($this->groupname_);
    print "</td></tr>\n";
    $switch = true;
    foreach( $matnr_array as $matnr ) {
      print "  <tr style=\"background-color:#";
      if( $switch ) {
	print "cccccc"; 
	$switch = false;
      }else{
	print "ffffff"; 
	$switch = true;
      }
      $result = $this->db_->selectFromStudent('*',$matnr['matnr']);
      print "\"><td>".$matnr['matnr']."</td><td>".$result['last_name']."</td><td>".$result['first_name']."</td>".
	"<td><a href=\"student.php?mode=form&matnr=".$matnr['matnr']."\">select</a></p></td></tr>\n";
    }
    print "</table>\n";
    
  }

  //------------------------------------------------------------------
  function createCard() {
    $this->createHeader();
    $this->coachingTableHeader();
    $this->createCoachingTable($this->db_->selectGroupCoaching($this->groupname_),$this->groupname_);
    $this->createNewCoachingEntry('',$this->groupname_);
    $this->coachingTableFooter();
  }

  //------------------------------------------------------------------
  function createCoachingEdit($timestamp) {
    $this->createHeader();
    $this->coachingTableHeader();
    print "<h2>Editiere Nachhilfeeintrag Für Gruppe: ".$this->groupname_."</h2>";
    $this->createCoachingEditTable($this->db_->selectGroupCoaching($this->groupname_,$timestamp),$this->groupname_);
    $this->coachingTableFooter();
  }
  
}

?>