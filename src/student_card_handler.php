<?php
  // ------------------------------------------------------------------
  // This is free software; you can redistribute it and/or modify
  // it under the terms of the GNU General Public License as published by
  // the Free Software Foundation; either version 2 of the License, or
  // (at your option) any later version.
  // 
  // This program is distributed in the hope that it will be useful,
  // but WITHOUT ANY WARRANTY; without even the implied warranty of
  // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  // GNU General Public License for more details.
  // 
  // You should have received a copy of the GNU General Public License
  // along with this program; if not, write to the Free Software
  // Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
  // 
  // Authors: Michael Hammer
  // ------------------------------------------------------------------*/

include_once("database_handler.php");
include_once("common.php");
include_once("html_form.php");
include_once("card_handler.php");
include_once("learngroup_selector.php");

class StudentCardHandler extends CardHandler {
  protected $learngroup_selector_;
  protected $matnr_;

  //------------------------------------------------------------------
  function __construct($db, $ldap, $matnr) {
    parent::__construct($db,$ldap);
    $this->matnr_=$matnr;
    $this->learngroup_selector_ = new LearngroupSelector($db);
  }

  //------------------------------------------------------------------
  protected function createHeader() {
    print "<h1><a href=\"student.php?mode=form&matnr=".$this->matnr_."\">".$this->db_->selectFromStudent('last_name',$this->matnr_).
      " ".$this->db_->selectFromStudent('first_name',$this->matnr_).
      " (".$this->matnr_."):</a></h1>\n";
    deleteStudentLink($this->matnr_);
    print "<p> Email: ".$this->db_->selectFromStudent('email',$this->matnr_)."</p>\n";    
  }

  //------------------------------------------------------------------
  protected function createLearngroupSelectorTable() {
    $student = $this->db_->selectStudent($this->matnr_);
    print "<table><tr>\n".
      "  <td>Hinzuf. zu</td><td>Gruppe</td>\n".
      "  <td></td>\n".
      "  <td width=100px>Matrikelnr.</td>\n".
      "  <td width=180px>Nachname</td>\n".
      "  <td width=180px>Vorname</td>\n".
      "  <td width=250px>Email</td>\n".
      "</tr>\n";
    print OTform("writer.php?target=learngroups&mode=edit&matnr=".$this->matnr_,"post").
      "<tr style=\"background-color:#cccccc\">\n";
    print $this->learngroup_selector_->tabs()."\n".
      "  <td>".
      input("submit","save","Speichern")."</td>\n".
      "  <td>".$this->matnr_."</td><td>".$student['last_name']."</td>\n".
      "  <td>".$student['first_name']."</td><td>".$student['email']."</td>\n".
      "  <td><a href=\"student.php?mode=form&matnr=".$this->matnr_."\">select</a></td>\n".
      "</tr>\n".CTform();
    print "</table>\n";
    $groups = $this->db_->findLearngroupsMembership($this->matnr_);
    if( $groups ) {
      print "<p>Mitglied in den Lerngruppen: <b>";
      foreach( $groups as $group ) {
	print $group['groupname']." ";
      }
      print "</b></p>\n";
    }
  }

  //------------------------------------------------------------------
  function createCard() {
    $this->createHeader();
    $this->createLearngroupSelectorTable();
    $this->coachingTableHeader();
    $this->createCoachingTable($this->db_->selectStudentCoaching($this->matnr_));
    $this->createNewCoachingEntry($this->matnr_);
    $this->coachingTableFooter();
  }

  //------------------------------------------------------------------
  function createCoachingEdit($timestamp) {
    $this->createHeader();
    print "<h2>Editiere Nachhilfeeintrag: </h2>";
    $this->coachingTableHeader();
    $this->createCoachingEditTable($this->db_->selectStudentCoaching($this->matnr_,$timestamp));
    $this->coachingTableFooter();
  }

  }

?>