<?php
  // ------------------------------------------------------------------
  // This is free software; you can redistribute it and/or modify
  // it under the terms of the GNU General Public License as published by
  // the Free Software Foundation; either version 2 of the License, or
  // (at your option) any later version.
  // 
  // This program is distributed in the hope that it will be useful,
  // but WITHOUT ANY WARRANTY; without even the implied warranty of
  // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  // GNU General Public License for more details.
  // 
  // You should have received a copy of the GNU General Public License
  // along with this program; if not, write to the Free Software
  // Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
  // 
  // Authors: Michael Hammer
  // ------------------------------------------------------------------*/

//------------------------------------------------------------------
function createHeaderMenu(){
  print "<body>\n<p align=\"right\">\n".
    "<a href=\"student.php?mode=form&learngroups=1\">Lerngruppen</a> | \n".
    "<a href=\"import_export.php?mode=import&target=student\">Import - Erstellung</a> | \n".
    "<a href=\"index.php\">Home</a>\n".
    "</p>\n";
}

//------------------------------------------------------------------
function convertSeconds($time_sec) {
  return( array( 'min' => round($time_sec/60), 'sec' => $time_sec % 60 ) );
}

//------------------------------------------------------------------
function deleteStudentLink($matnr) {
  print "<a href=\"javascript:deleteStudent('".$matnr."')\">Lösche Student</a>";
}

//------------------------------------------------------------------
function deleteGroupLink($groupname) {
  print "<a href=\"javascript:deleteGroup('".$groupname."')\">Lösche Gruppe</a>";
}

//------------------------------------------------------------------
function createFooter() { 
  print "<br/><br/><hr>Copyright by Michael Hammer - Institut für Festigkeitslehre - <a href=\"http://stoodle.fest.tugraz.at/wiki\">Wiki</a></body>"."\n".
    "</html>"."\n";
  die();
}

?>