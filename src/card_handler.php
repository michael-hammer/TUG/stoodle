<?php
  // ------------------------------------------------------------------
  // This is free software; you can redistribute it and/or modify
  // it under the terms of the GNU General Public License as published by
  // the Free Software Foundation; either version 2 of the License, or
  // (at your option) any later version.
  // 
  // This program is distributed in the hope that it will be useful,
  // but WITHOUT ANY WARRANTY; without even the implied warranty of
  // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  // GNU General Public License for more details.
  // 
  // You should have received a copy of the GNU General Public License
  // along with this program; if not, write to the Free Software
  // Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
  // 
  // Authors: Michael Hammer
  // ------------------------------------------------------------------*/

include_once("common.php");

class CardHandler {
  protected $db_;
  protected $ldap_;

  //------------------------------------------------------------------
  function __construct($db,$ldap) {
    $this->db_=$db;
    $this->ldap_=$ldap;
  }

  //------------------------------------------------------------------
  protected function coachingTableHeader() {
    print "<h2>Nachhilfe</h2>\n<table>\n";
    print "<tr>".
      "<td>Datum</td>".
      "<td>VO</td>".
      "<td>UE</td>".
      "<td>Dauer</td>".
      "<td>Tutor</td>".
      "<td>Kommentar</td>".
      "</tr>\n";
  }

  //------------------------------------------------------------------
  protected function coachingTableFooter() {
    print "</table>\n";
  }

  //------------------------------------------------------------------
  protected function createTutorOption($tutors,$selected) {
    print "  <select name=\"tutor\">\n";
    foreach ($tutors as $tutor) {
      print "    <option value=\"".$tutor['uid']."\"";
      if( $tutor['uid'] == $selected )
	print " selected=\"selected\"";
      print ">".$tutor['last_name']."</option>"."\n";
    }
    print "  </select>\n";
  }

  //------------------------------------------------------------------
  protected function createDurationOption($durations,$selected) {
    print "  <select name=\"duration\">\n";
    foreach ($durations as $row) {
      print "    <option value=\"".$row['time']."\">".$row['name']."</option>\n";
    }
    print "  </select>";
  }

  //------------------------------------------------------------------
  protected function createNewCoachingEntry($matnr,$groupname=null) {
    print OTform("writer.php?target=coaching&mode=new","post").
      "<tr>\n".
      "  <td>".$this->db_->getDateTime()."</td>\n".
      "  <td align=\"center\">".input("checkbox","vo","1")."</td>\n".
      "  <td align=\"center\">".input("checkbox","ue","1")."</td>\n".
      "  <td align=\"right\">\n";
    $this->createDurationOption( $this->db_->selectDurationTypes() , 'Kurz' );
    print "  </td>\n".
      "  <td>\n";
    $this->createTutorOption( $this->ldap_->getTutors() , $_SERVER['PHP_AUTH_USER'] );
    print "  </td>"."\n".
      "  <td>".input("text","comment","","50")."</td>\n".
      "  <td>".input("submit","save","Speichern").
      ( $groupname ? input("hidden","groupname",$groupname) : input("hidden","matnr",$matnr) )."</td>\n".
      "</tr>\n".
      CTform();
  }

  //------------------------------------------------------------------
  protected function createCoachingTable($array,$groupname=null) {
    foreach ($array as $row) {
      $tutor = $this->ldap_->getPersonalData($row['tutor']);
      $time = convertSeconds($row['duration']);

      print "<tr style=\"background-color:#".$this->db_->getDurationColour($row['duration'])."\">\n".
	"  <td>".$this->db_->getDateTime($row['date'])."</td>\n".
	"  <td align=\"center\">".input("checkbox","vo",'1','','disabled="disabled"'.( $row['vo'] ? ' checked="checked"' : ''))."</td>\n".
	"  <td align=\"center\">".input("checkbox","ue",'1','','disabled="disabled"'.( $row['ue'] ? ' checked="checked"' : ''))."</td>\n".
	"  <td align=\"right\">".$time['min']." min ".$time['sec']." sec</td>\n".
	"  <td>".$tutor['last_name']."</td>\n".
	"  <td>".$row['comments']."</td>\n".
	"  <td><a href=\"student.php?mode=edit_coaching&timestamp=".$row['date'].( $groupname ? "&groupname=".$groupname : "&matnr=".$row['matnr'] )."\">edit</a> / ".
	"<a href=\"writer.php?target=coaching&mode=delete&timestamp=".$row['date'].( $groupname ? "&groupname=".$groupname : "&matnr=".$row['matnr'] )."\">delete</a></td>\n".
	"<tr>\n";
      if( $groupname ) {
	print "<tr><td align=\"right\">Studenten:</td><th colspan=\"6\">";
	$matnr_array = $this->db_->findLearngroupsMatnr($groupname);
	foreach( $matnr_array as $matnr ) {
	  print ( $this->db_->askStudentInCoaching($matnr['matnr'],$row) ? $matnr['matnr']." " : "" );
	}
	print "</tr>\n";
      }
      print "</tr>\n";
    }
  }

  //------------------------------------------------------------------
  function createCoachingEditTable($data,$groupname=null) {
    $duration = convertSeconds($data['duration']);
    print OTform("writer.php?target=coaching&mode=edit","post").
      "<tr>\n".
      "  <td>".$this->db_->getDateTime($data['date'])."</td>\n".
      "  <td align=\"center\">".input("checkbox","vo",'','',( $data['vo'] ? ' checked="checked"' : ''))."</td>\n".
      "  <td align=\"center\">".input("checkbox","ue",'','',( $data['ue'] ? ' checked="checked"' : ''))."</td>\n".      
      "  <td>".input('text','duration_m',$duration['min'],'3')."min ".input('text','duration_s',$duration['sec'],'2')."sec </td>\n".
      "  <td>\n";
    $this->createTutorOption( $this->ldap_->getTutors() , $data['tutor'] );
    print "  </td>\n".
      "  <td>".input('text','comment',$data['comments'],'50')."</td>\n".
      "  <td>".input("submit","save","Speichern").
      ( $groupname ? input("hidden","groupname",$groupname) : input("hidden","matnr",$data['matnr']) ).
      input("hidden","timestamp",$data['date'])."</td>\n".
      "</tr>\n".
      CTform();
  }

  }

?>