function deleteStudent( matnr ) {
    Check = confirm("Wollen Sie den Studierenden mit "+matnr+ " wirklich löschen?");
    if( Check == true )  {
	window.location.href = "writer.php?target=student&mode=delete&matnr="+matnr;
    }
}

function deleteGroup( groupname ) {
    Check = confirm("Wollen Sie die Lerngruppe "+groupname+ " wirklich löschen?");
    if( Check == true )  {
	window.location.href = "writer.php?target=learngroups&mode=delete&groupname="+groupname;
    }
}
