<?php
  // ------------------------------------------------------------------
  // This is free software; you can redistribute it and/or modify
  // it under the terms of the GNU General Public License as published by
  // the Free Software Foundation; either version 2 of the License, or
  // (at your option) any later version.
  // 
  // This program is distributed in the hope that it will be useful,
  // but WITHOUT ANY WARRANTY; without even the implied warranty of
  // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  // GNU General Public License for more details.
  // 
  // You should have received a copy of the GNU General Public License
  // along with this program; if not, write to the Free Software
  // Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
  // 
  // Authors: Michael Hammer
  // ------------------------------------------------------------------*/

include_once("database_handler.php");
include_once("common.php");
include_once("html_form.php");
include_once("learngroup_selector.php");

class StudentSelector {
  protected $learngroup_selector_;
  protected $db_;

  //------------------------------------------------------------------
  function __construct($db) {
    $this->db_=$db;
    $this->learngroup_selector_ = new LearngroupSelector($db);
  }

  //------------------------------------------------------------------
  protected function createHeader() {
    print "<h1>Wählen Sie Studenten oder Lerngruppe:</h1>"."\n";
  }

  //------------------------------------------------------------------
  protected function createTable($array,$group=null) {
    $switch = true;
    print "<table><tr>\n".
      ( $group ? "  <td width=70px>Löschen</td><td>Hinzuf.</td>\n" : "  <td>Hinzuf. zu</td><td>Gruppe</td>\n") .
      "  <td></td>\n".
      "  <td width=100px>Matrikelnr.</td>\n".
      "  <td width=180px>Nachname</td>\n".
      "  <td width=180px>Vorname</td>\n".
      "  <td width=250px>Email</td>\n".
      "</tr>\n";
    foreach ($array as $row) {
      print OTform("writer.php?target=learngroups&mode=edit&matnr=".$row['matnr'],"post").
	"<tr style=\"background-color:#";
      if( $switch ) {
	print "cccccc"; 
	$switch = false;
      }else{
	print "ffffff"; 
	$switch = true;
      }
      print "\">\n";
      print ( $group ? $this->learngroup_selector_->deleteTabs($group) : $this->learngroup_selector_->tabs() )."\n".
	"  <td>".
	input("hidden","search",$_POST['search']).
	input("hidden","matnr",$_POST['matnr']).
	input("hidden","last_name",$_POST['last_name']).
	input("hidden","first_name",$_POST['first_name']).
	input("submit","save","Speichern")."</td>\n".
	"  <td>".$row['matnr']."</td><td>".$row['last_name']."</td>\n".
	"  <td>".$row['first_name']."</td><td>".$row['email']."</td>\n".
	"  <td><a href=\"student.php?mode=form&matnr=".$row['matnr']."\">select</a></td>\n".
	"</tr>\n".
	CTform();
    }
    print "</table>\n";    
  }

  //------------------------------------------------------------------
  protected function getGroupData($search_students) {
    $group_data = array();
    foreach( $search_students as $student ) {
      $groups = $this->db_->selectLearngroupsMatnr($student['matnr']);
      foreach( $groups as $row )
	$group_data[$row['groupname']] = array();
    }

    foreach( $group_data as $groupname => $matnr ) {
      $member_array = $this->db_->selectLearngroupsGroupname($groupname);
      foreach( $member_array as $member ) {
	$group_data[$groupname][] = $member['matnr'];
      }
    }
    return( $group_data );
  }

  //------------------------------------------------------------------
  protected function getAllGroupData() {
    $group_data = array();
    $groupnames = $this->db_->findLearngroupsGroupnames();
    foreach( $groupnames as $groupname ) {
      $member_array = $this->db_->selectLearngroupsGroupname($groupname['groupname']);
      foreach( $member_array as $member ) {
	$group_data[$groupname['groupname']][] = $member['matnr'];
      }
    }
    return( $group_data );
  }

  //------------------------------------------------------------------
  function createGroupTable($groups) {
    if( $groups ) {
      print "<h2>Lerngruppentabelle:</h2>\n";
      foreach( $groups as $group_name => $members ) {
	print "<p><b>".$group_name."</b> ";
	deleteGroupLink($group_name);
	print "/ <a href=\"student.php?mode=form&groupname=".$group_name."\">select</a></p>\n";
	$student_array = array();
	foreach( $members as $matnr ) {
	  $student_array[] = $this->db_->selectStudent($matnr);
	}
	$this->createTable($student_array,$group_name);
      }
    }
  }

  //------------------------------------------------------------------
  function createSearchTable($search_students) {
    print "<h2>Suchergebnis</h2>\n";
    $this->createTable($search_students);
  }

  //------------------------------------------------------------------
  function createSelection($string,$config_search) {
    $search_students = $this->db_->findMatchingStudents($string,$config_search);
      
    $this->createHeader();
    $this->createGroupTable($this->getGroupData($search_students));
    $this->createSearchTable($search_students);
  }

  //------------------------------------------------------------------
  function createLearngroupsSelection() {
    $this->createHeader();
    $this->createGroupTable($this->getAllGroupData());
  }
  
  }

?>