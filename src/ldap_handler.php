<?php
  // ------------------------------------------------------------------
  // This is free software; you can redistribute it and/or modify
  // it under the terms of the GNU General Public License as published by
  // the Free Software Foundation; either version 2 of the License, or
  // (at your option) any later version.
  // 
  // This program is distributed in the hope that it will be useful,
  // but WITHOUT ANY WARRANTY; without even the implied warranty of
  // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  // GNU General Public License for more details.
  // 
  // You should have received a copy of the GNU General Public License
  // along with this program; if not, write to the Free Software
  // Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
  // 
  // Authors: Michael Hammer
  // ------------------------------------------------------------------*/

class LDAPHandler {
  protected $server_="localhost";
  protected $port_=389;
  protected $dn_="dc=fest.tugraz,dc=at";
  protected $connecton_;

  //------------------------------------------------------------------
  function __construct() {
    $this->connection_ = ldap_connect( $this->server_, $this->port_ );
    ldap_set_option($this->connection_, LDAP_OPT_PROTOCOL_VERSION, 3);
    ldap_bind($this->connection_);
  }

  //------------------------------------------------------------------
  function __destruct() {
    ldap_close($this->connection_);
  }

  //------------------------------------------------------------------
  function getTutors() {
    $b="cn=stoodle,ou=Group,$this->dn_";
    $filter="(memberUid=*)";
    $values=array("memberUid");   
    $result=ldap_search($this->connection_,$b,$filter,$values);
    $tutors=array();
    if( $result ) {
      $data=ldap_get_entries($this->connection_,$result);
      for($count = 0; $count < $data['0']['memberuid']['count'] ; $count++ ) {
	$p_data=$this->getPersonalData($data['0']['memberuid'][$count]);
	$tutors[] = array( uid=>$data['0']['memberuid'][$count] ,
			   last_name=>$p_data['last_name'],
			   first_name=>$p_data['first_name']);
      }      
    }
    return($tutors);
  }

  //------------------------------------------------------------------
  function getPersonalData($uid) {
    $filter="(uid=$uid)";
    $values=array("givenName","sn");
    $result=ldap_search($this->connection_,$this->dn_,$filter,$values);
    if( $result ) {
      $data=ldap_get_entries($this->connection_,$result);
      return( array( 'last_name'=>$data['0']['sn']['0'] , 'first_name'=>$data['0']['givenname']['0'] ) );
    }
    else
      return( array("","") );
  }

  }

?>