<?php
  // ------------------------------------------------------------------
  // This is free software; you can redistribute it and/or modify
  // it under the terms of the GNU General Public License as published by
  // the Free Software Foundation; either version 2 of the License, or
  // (at your option) any later version.
  // 
  // This program is distributed in the hope that it will be useful,
  // but WITHOUT ANY WARRANTY; without even the implied warranty of
  // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  // GNU General Public License for more details.
  // 
  // You should have received a copy of the GNU General Public License
  // along with this program; if not, write to the Free Software
  // Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
  // 
  // Authors: Michael Hammer
  // ------------------------------------------------------------------*/

include_once("common.php");

class LearngroupSelector {
  protected $db_;

  //------------------------------------------------------------------
  function __construct($db) {
    $this->db_ = $db;
  }

  //------------------------------------------------------------------
  function input() {
    return input("text","group_new","","8");
  }

  //------------------------------------------------------------------
  function option() {
    $learngroups = $this->db_->findLearngroupsGroupnames();
    $temp = "  <select name=\"group_option\">\n".
      "    <option>---</option>\n";
    foreach ($learngroups as $groupname) {
      $temp .= "    <option value=\"".$groupname['groupname']."\"".
	">".$groupname['groupname']."</option>"."\n";
    }
    $temp .= "  </select>\n";
    return( $temp );
  }

  //------------------------------------------------------------------
  function tabs() {
    return("<td align=\"center\">".$this->input()."</td><td align=\"center\">\n".$this->option()."</td>");
  }

  //------------------------------------------------------------------
  function deleteTabs($group) {
    return("<td align=\"center\">".input("checkbox","deletegroup","").input("hidden","group","$group","")."</td><td align=\"center\">".$this->option()."</td>");
  }

  }

?>