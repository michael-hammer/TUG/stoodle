<?php
  // ------------------------------------------------------------------
  // This is free software; you can redistribute it and/or modify
  // it under the terms of the GNU General Public License as published by
  // the Free Software Foundation; either version 2 of the License, or
  // (at your option) any later version.
  // 
  // This program is distributed in the hope that it will be useful,
  // but WITHOUT ANY WARRANTY; without even the implied warranty of
  // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  // GNU General Public License for more details.
  // 
  // You should have received a copy of the GNU General Public License
  // along with this program; if not, write to the Free Software
  // Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
  // 
  // Authors: Michael Hammer
  // ------------------------------------------------------------------*/

class DatabaseHandler {
  protected $db_name_;
  protected $db_;
  protected $date_string_="%d.%m.%Y %H:%M:%S";

  //------------------------------------------------------------------
  function __construct($db_name) {
    $this->db_name_=$db_name;
    try {
      $this->db_ = new PDO("sqlite:$db_name");
      $this->db_->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      $temp = $this->db_->query("SELECT tbl_name FROM sqlite_master WHERE tbl_name = 'student';")->fetch(PDO::FETCH_ASSOC);
      if( ! $temp )
	$this->db_->exec("CREATE TABLE student (matnr TEXT, last_name TEXT, first_name TEXT, email TEXT)");
     
      $temp = $this->db_->query("SELECT tbl_name FROM sqlite_master WHERE tbl_name = 'coaching';")->fetch(PDO::FETCH_ASSOC);
      if( ! $temp )
	$this->db_->exec("CREATE TABLE coaching (matnr TEXT, date INTEGER, vo TEXT, ue TEXT, tutor TEXT, duration INTEGER, comments TEXT);");

      $temp = $this->db_ -> query("SELECT tbl_name FROM sqlite_master WHERE tbl_name = 'duration';")->fetch(PDO::FETCH_ASSOC);
      if( ! $temp )
	$this->db_->exec("CREATE TABLE duration (name TEXT, time INTEGER, colour TEXT);
                      INSERT INTO duration VALUES ('Kurz',1200,'00ff00');
                      INSERT INTO duration VALUES ('Mittel',2400,'ffff00');
                      INSERT INTO duration VALUES ('Lang',14400,'ff0000');");

      $temp = $this->db_ -> query("SELECT tbl_name FROM sqlite_master WHERE tbl_name = 'learngroups';")->fetch(PDO::FETCH_ASSOC);
      if( ! $temp )
	$this->db_->exec("CREATE TABLE learngroups (groupname TEXT, matnr TEXT);");
    } catch(PDOException $e) {
      print "Error: " . $e->getMessage() . "<br/>";
    }
    
    $this->insert_coaching_ = 
      $this->db_->prepare("INSERT INTO coaching VALUES (:matnr,:timestamp,:vo,:ue,:tutor,:duration,:comment);");
    $this->insert_student_ =
      $this->db_->prepare("INSERT INTO student VALUES (?,?,?,?)");
    $this->insert_learngroups_ =
      $this->db_->prepare("INSERT INTO learngroups VALUES (?,?)");

    $this->delete_student_ =
      $this->db_->prepare("DELETE FROM student WHERE matnr = ?;");
    $this->delete_coaching_ =
      $this->db_->prepare("DELETE FROM coaching WHERE matnr = ?");    
    $this->delete_learngroups_entry_ = 
      $this->db_->prepare("DELETE FROM learngroups WHERE groupname = ? AND matnr = ?");
    $this->delete_learngroups_ = 
      $this->db_->prepare("DELETE FROM learngroups WHERE groupname = ?");
    $this->delete_learngroups_matnr_ = 
      $this->db_->prepare("DELETE FROM learngroups WHERE matnr = ?");
    
    $this->update_coaching_ =
      $this->db_->prepare("UPDATE coaching SET vo = :vo,ue = :ue,tutor = :tutor,duration = :duration,comments = :comment WHERE matnr = :matnr AND date = :timestamp");
    $this->update_student_ =
      $this->db_->prepare("UPDATE student SET last_name = ?,first_name = ?,email = ? WHERE matnr = ?");

    $this->select_all_from_student_ =
      $this->db_->prepare("SELECT * FROM student WHERE matnr = ?");
    $this->select_all_from_coaching_ =
      $this->db_->prepare("SELECT * FROM coaching WHERE matnr = ?");
    $this->select_all_from_coaching_where_matnr_and_timestamp_ =
      $this->db_->prepare("SELECT * FROM coaching WHERE matnr = ? AND date = ?");
    $this->select_from_coaching_where_groupname_ = 
      $this->db_->prepare("SELECT DISTINCT date,vo,ue,tutor,duration,comments FROM coaching WHERE matnr IN (SELECT matnr FROM learngroups WHERE groupname = :groupname)");
    $this->select_from_coaching_where_groupname_and_timestamp_ = 
      $this->db_->prepare("SELECT DISTINCT date,vo,ue,tutor,duration,comments FROM coaching WHERE matnr IN (SELECT matnr FROM learngroups WHERE groupname = :groupname) AND date = :timestamp");
    $this->select_from_coaching_where_all_ =
      $this->db_->prepare("SELECT * FROM coaching WHERE matnr = :matnr AND date = :date AND duration = :duration AND tutor = :tutor AND comments = :comments");
    $this->select_all_from_learngroups_where_matnr_ =
      $this->db_->prepare("SELECT * FROM learngroups WHERE matnr = ?");
    $this->select_all_from_learngroups_where_groupname_ =
      $this->db_->prepare("SELECT * FROM learngroups WHERE groupname = ?");    
    $this->select_all_from_learngroups_where_groupname_and_matnr_ =
      $this->db_->prepare("SELECT * FROM learngroups WHERE groupname = ? AND matnr = ?");
    $this->select_distinct_groupname_from_learngroups_ =
      $this->db_->prepare("SELECT DISTINCT groupname FROM learngroups");
    $this->select_distinct_groupname_from_learngroups_where_matnr_ =
      $this->db_->prepare("SELECT DISTINCT groupname FROM learngroups WHERE matnr = ?");
    $this->select_matnr_from_learngroups_where_groupname_ =
      $this->db_->prepare("SELECT matnr FROM learngroups WHERE groupname = ?");
  }

  //------------------------------------------------------------------
  function __destruct() {
    $this->db_ = null;
  }

  //------------------------------------------------------------------
  protected function createSearchStatement($string,$config_search) {
    $str = "SELECT * FROM student WHERE ";
    $first = true;
    if( $config_search['matnr'] ) {
      $first = false;
      $str .= "matnr LIKE '$string'";
    }
    if( $config_search['last_name'] ) {
      if( ! $first )
	$str .= " OR ";
      $first = false;
      $str .= "last_name LIKE '$string'";
    }
    if( $config_search['first_name'] ) {
      if( ! $first )
	$str .= " OR ";
      $first = false;
      $str .= "first_name LIKE '$string'";
    }
    $str .= " ORDER BY last_name ASC, first_name ASC";
    return( $this->db_->prepare($str) );
  }

  //------------------------------------------------------------------
  /**
     Returns the single result in a column for an matnr from the student table
   */
  function selectFromStudent($column,$matnr) {
    $select_from_student = 
      $this->db_->prepare('SELECT '.$column.' FROM student WHERE matnr = "'.$matnr.'"');
    try {
      $select_from_student->execute();
      $arr = $select_from_student->fetch(PDO::FETCH_ASSOC);
    } catch(PDOException $e) {
      print "Error: " . $e->getMessage() . "<br/>";
      return;
    }
    if( $column == "*" )
      return( $arr );
    else
      return( $arr[$column] );
  }

  //-----------------------------------------------------------------
  /**
     Finds all entries matching the LIKE string and returns the amount
   */
  function countMatchingStudents($string,$config_search) {
    try {
      $statement = $this->createSearchStatement($string,$config_search);
      $statement->execute();
    } catch(PDOException $e) {
      print "Error: " . $e->getMessage() . "<br/>";
      return;
    }

    while( $arr = $statement -> fetch(PDO::FETCH_ASSOC) )
      $counter += 1;
    return( $counter );
  }

  //------------------------------------------------------------------
  /**
     Finds all entries matching the LIKE string and returns an array with the personal data
   */
  function findMatchingStudents($string,$config_search) {
    try {
      $statement = $this->createSearchStatement($string,$config_search);
      $statement->execute();
    } catch(PDOException $e) {
      print "Error: " . $e->getMessage() . "<br/>";
      return( 0 );
    }

    $arr = $statement -> fetchAll(PDO::FETCH_ASSOC);
    return( $arr );
  }


  //------------------------------------------------------------------
  /**
     Finds and returns the first MatNr for the given LIKE string
   */
  function findSingleMatNr($string,$config_search) {
    try {
      $statement = $this->createSearchStatement($string,$config_search);
      $statement->execute();
    } catch(PDOException $e) {
      print "Error: " . $e->getMessage() . "<br/>";
      return( 0 );
    }
    $arr = $statement -> fetch(PDO::FETCH_ASSOC);
    return( $arr['matnr'] );
  }

  //------------------------------------------------------------------
  /**
     insert Student data and sets the merging strategy - the newer wins and overwrites personal data!
   */
  function selectStudent($matnr) {
    try {
      $this->select_all_from_student_->execute(array($matnr));
      $arr = $this->select_all_from_student_->fetch(PDO::FETCH_ASSOC);
    } catch(PDOException $e) {
      print "Error: " . $e->getMessage() . "<br/>";
      return;
    } 
    return( $arr );
  }

  //------------------------------------------------------------------
  /**
     insert Student data and sets the merging strategy - the newer wins and overwrites personal data!
   */
  function insertStudent($data) {
    try {
      $this->select_all_from_student_->execute(array($data['matnr']));
      if( $this->select_all_from_student_->fetch(PDO::FETCH_ASSOC) ) {
	$this->update_student_->execute(array($data['last_name'],$data['first_name'],$data['email'],$data['matnr']));
      }else{
	$this->insert_student_->execute(array($data['matnr'],$data['last_name'],$data['first_name'],$data['email']));
      }
    } catch(PDOException $e) {
      print "Error: " . $e->getMessage() . "<br/>";
      return;
    } 
  }

  //------------------------------------------------------------------
  /**
     deletes Student (and all related data) from database
   */
  function deleteStudent($matnr) {
    try {
      $this->delete_student_->execute(array($matnr));
      $this->delete_coaching_->execute(array($matnr));
      $this->delete_learngroups_matnr_->execute(array($matnr));
    } catch(PDOException $e) {
      print "Error: " . $e->getMessage() . "<br/>";
      return;
    }       
  }

  //------------------------------------------------------------------
  /**
     Returns an array containing all coaching dates for Student (the matnr has to exist!)
   */
  function selectStudentCoaching($matnr,$timestamp = null) {
    try {
      if( $timestamp ) {
	$this->select_all_from_coaching_where_matnr_and_timestamp_->execute(array($matnr,$timestamp));
	$arr = $this->select_all_from_coaching_where_matnr_and_timestamp_->fetch(PDO::FETCH_ASSOC);
      }
      else {
	$this->select_all_from_coaching_->execute(array($matnr));
	$arr = $this->select_all_from_coaching_->fetchAll();
      }
    } catch(PDOException $e) {
      print "Error: " . $e->getMessage() . "<br/>";
      return;
    }
    return( $arr );
  }

  //------------------------------------------------------------------
  /**
     Returns an array containing all coaching dates for learngroup (the learngroup has to exist!)
   */
  function selectGroupCoaching($groupname,$timestamp = null) {
    try {
      if( $timestamp ) {
	$this->select_from_coaching_where_groupname_and_timestamp_->execute(array('groupname'=>$groupname,'timestamp'=>$timestamp));
	$arr = $this->select_from_coaching_where_groupname_and_timestamp_->fetch(PDO::FETCH_ASSOC);
      }
      else {
	$this->select_from_coaching_where_groupname_->execute(array(':groupname'=>$groupname));
	$arr = $this->select_from_coaching_where_groupname_->fetchAll();
      }
    } catch(PDOException $e) {
      print "Error: " . $e->getMessage() . "<br/>";
      return;
    }
    return( $arr );
  }

  //------------------------------------------------------------------
  function askStudentInCoaching($matnr,$data) {
    try {
      $this->select_from_coaching_where_all_->execute(array(':matnr'=>$matnr,
							    ':date'=>$data['date'],
							    ':duration'=>$data['duration'],
							    ':tutor'=>$data['tutor'],
							    ':comments'=>$data['comments']));
      $arr = $this->select_from_coaching_where_all_->fetch(PDO::FETCH_ASSOC);
    } catch(PDOException $e) {
      print "Error: " . $e->getMessage() . "<br/>";
      return;
    }
    return( ($arr ? true : false ) );
  }

  //------------------------------------------------------------------
  /**
     Returns an array containing all defined duration types
   */
  function selectDurationTypes() {
    try {
      $temp = $this->db_->query("SELECT name,time FROM duration;");
    } catch(PDOException $e) {
      print "Error: " . $e->getMessage() . "<br/>";
      return;
    }
    if( $temp ) {
      return( $temp->fetchAll( ));
    }
    return;
  }

  //------------------------------------------------------------------
  /**
     Get Date and Time from database
   */
  function getDateTime($timestamp = null) {
    if( ! $timestamp ) 
      $temp = $this->db_->query("SELECT strftime('$this->date_string_','now','localtime');")->fetch(PDO::FETCH_BOTH);
    else
      $temp = $this->db_->query("SELECT strftime('$this->date_string_',$timestamp,'unixepoch','localtime');")->fetch(PDO::FETCH_BOTH);
    return($temp['0']);
  }

  //------------------------------------------------------------------
  function getNowTimestamp() {
    $temp = $this->db_->query("SELECT strftime('%s','now');")->fetch(PDO::FETCH_BOTH);
    return($temp['0']);
  }

  //------------------------------------------------------------------
  /**
     get colours for the given duration
   */
  function getDurationColour($duration) {
    try {
      $temp = $this->db_->query("SELECT time,colour FROM duration;");
    } catch(PDOException $e) {
      print "Error: " . $e->getMessage() . "<br/>";
      return;
    }
    if( $temp ) {
      while( $row = $temp -> fetch(PDO::FETCH_ASSOC) ) {
	if( $duration <= $row['time'] )
	  return( $row['colour'] );
      }
    }
  }

  //------------------------------------------------------------------
  /**
     inserts a new coaching entry
   */
  function insertCoaching($data) {
    try {
      $this->insert_coaching_->execute(array(':matnr'=>$data['matnr'],
					     ':timestamp'=>( $data['timestamp'] ? $data['timestamp'] : $this->getNowTimestamp() ),
					     ':vo'=>$data['vo'],
					     ':ue'=>$data['ue'],
					     ':tutor'=>$data['tutor'],
					     ':duration'=>$data['duration'],
					     ':comment'=>$data['comment']));
    } catch(PDOException $e) {
      print "Error: " . $e->getMessage() . "<br/>";
      return;
    }
  }

  //------------------------------------------------------------------
  /**
   updates coaching entry
   */
  function updateCoaching($data) {
    try {
      $this->update_coaching_->execute(array(':timestamp'=>$data['timestamp'],
					     ':matnr'=>$data['matnr'],
					     ':vo'=>$data['vo'],
					     ':ue'=>$data['ue'],
					     ':tutor'=>$data['tutor'],
					     ':duration'=>$data['duration'],
					     ':comment'=>$data['comment']));
    } catch(PDOException $e) {
      print "Error: " . $e->getMessage() . "<br/>";
      return;
    }
  }

  //------------------------------------------------------------------
  /**
     deletes a coaching entry given by the rowid in coaching table
   */
  function deleteCoaching($matnr,$timestamp) {
    try {
      $this->db_->exec("DELETE FROM coaching WHERE matnr = '$matnr' AND date = '$timestamp'");
    } catch(PDOException $e) {
      print "Error: " . $e->getMessage() . "<br/>";
      return;
    }
  }

  //------------------------------------------------------------------
  /**
   insert Learngroup entry if not already exists
   */
  function insertLearngroups($data) {
    try {
      $this->select_all_from_learngroups_where_groupname_and_matnr_->execute(array($data['groupname'],$data['matnr']));
      if( $this->select_all_from_learngroups_where_groupname_and_matnr_->fetch(PDO::FETCH_ASSOC) )
	return;
      $this->insert_learngroups_->execute(array($data['groupname'],$data['matnr']));
    } catch(PDOException $e) {
      print "Error: " . $e->getMessage() . "<br/>";
      return;
    }
  }

  //------------------------------------------------------------------
  /**
     returns distinct learngroups groupnames
   */
  function findLearngroupsGroupnames() {
    try {
      $this->select_distinct_groupname_from_learngroups_->execute();
      $arr = $this->select_distinct_groupname_from_learngroups_->fetchAll(PDO::FETCH_ASSOC);
    } catch(PDOException $e) {
      print "Error: " . $e->getMessage() . "<br/>";
      return;
    }
    return( $arr );
  }

  //------------------------------------------------------------------
  function findLearngroupsMembership($matnr) {
    try {
      $this->select_distinct_groupname_from_learngroups_where_matnr_->execute(array($matnr));
      $arr = $this->select_distinct_groupname_from_learngroups_where_matnr_->fetchAll(PDO::FETCH_ASSOC);
    } catch(PDOException $e) {
      print "Error: " . $e->getMessage() . "<br/>";
      return;
    }
    return( $arr );    
  }

  //------------------------------------------------------------------
  function findLearngroupsMatnr($groupname) {
    try {
      $this->select_matnr_from_learngroups_where_groupname_->execute(array($groupname));
      $arr = $this->select_matnr_from_learngroups_where_groupname_->fetchAll(PDO::FETCH_ASSOC);
    } catch(PDOException $e) {
      print "Error: " . $e->getMessage() . "<br/>";
      return;
    }
    return( $arr );
  }

  //------------------------------------------------------------------
  /*
   select learngroups where matnr matches.
   */
  function selectLearngroupsMatnr($matnr) {
    try {
      $this->select_all_from_learngroups_where_matnr_->execute(array($matnr));
      $arr = $this->select_all_from_learngroups_where_matnr_->fetchAll(PDO::FETCH_ASSOC);
    } catch(PDOException $e) {
      print "Error: " . $e->getMessage() . "<br/>";
      return;
    }
    return( $arr );
  }

  //------------------------------------------------------------------
  /*
   select learngroups where groupname matches.
   */
  function selectLearngroupsGroupname($groupname) {
    try {
      $this->select_all_from_learngroups_where_groupname_->execute(array($groupname));
      $arr = $this->select_all_from_learngroups_where_groupname_->fetchAll(PDO::FETCH_ASSOC);
    } catch(PDOException $e) {
      print "Error: " . $e->getMessage() . "<br/>";
      return;
    }
    return( $arr );
  }

  //------------------------------------------------------------------
  function deleteLearngroupsEntry($data) {
    try {
      $this->delete_learngroups_entry_->execute(array($data['groupname'],$data['matnr']));
    } catch(PDOException $e) {
      print "Error: " . $e->getMessage() . "<br/>";
      return;
    }
  }

  //------------------------------------------------------------------
  function deleteLearngroups($groupname) {
    try {
      $this->delete_learngroups_->execute(array($groupname));
    } catch(PDOException $e) {
      print "Error: " . $e->getMessage() . "<br/>";
      return;
    }    
  }

  }
?>