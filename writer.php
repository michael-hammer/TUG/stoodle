<?php
  // ------------------------------------------------------------------
  // This is free software; you can redistribute it and/or modify
  // it under the terms of the GNU General Public License as published by
  // the Free Software Foundation; either version 2 of the License, or
  // (at your option) any later version.
  // 
  // This program is distributed in the hope that it will be useful,
  // but WITHOUT ANY WARRANTY; without even the implied warranty of
  // MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  // GNU General Public License for more details.
  // 
  // You should have received a copy of the GNU General Public License
  // along with this program; if not, write to the Free Software
  // Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
  // 
  // Authors: Michael Hammer
  // ------------------------------------------------------------------*/

include("htdocs/header.html");

include_once("src/database_handler.php");
include_once("src/common.php");

$db = new DatabaseHandler('db/database.db');

switch( $_GET['target'] ) {
 case 'coaching':
   $data = array( 'matnr'=>'',
		  'vo'=>$_POST['vo'],
		  'ue'=>$_POST['ue'],
		  'duration'=>$_POST['duration'],
		  'tutor'=>$_POST['tutor'],
		  'comment'=>$_POST['comment']);
   switch( $_GET['mode'] ) {
   case 'new':
     if( $_POST['groupname'] ) {
       $matnr_array = $db->findLearngroupsMatnr($_POST['groupname']);
       $data['timestamp'] = $db->getNowTimestamp();
       foreach( $matnr_array as $matnr ) {
	 $data['matnr'] = $matnr['matnr'];
	 $db->insertCoaching($data);
       }     
       print "<body onload=\"window.location.href='student.php?mode=form&groupname=".$_POST['groupname']."'\")>\n";
       createFooter();
     }
     if( $_POST['matnr'] ) {
       $data['matnr']=$_POST['matnr'];
       $db->insertCoaching($data);
       print "<body onload=\"window.location.href='student.php?mode=form&matnr=".$_POST['matnr']."'\")>\n";
       createFooter();
     }
     break;
   case 'edit':
     $data['timestamp']=$_POST['timestamp'];
     $data['duration']=$_POST['duration_m']*60+$_POST['duration_s'];
     if( $_POST['groupname'] ) {
       $matnr_array = $db->findLearngroupsMatnr($_POST['groupname']);
       foreach( $matnr_array as $matnr ) {
	 $data['matnr'] = $matnr['matnr'];
	 $db->updateCoaching($data);
	 print_r($data);
       } 
       print "<body onload=\"window.location.href='student.php?mode=form&groupname=".$_POST['groupname']."'\")>\n";
       createFooter();
     }
     if( $_POST['matnr'] ) {    
       $data['matnr']=$_POST['matnr'];
       $db->updateCoaching($data);
       print "<body onload=\"window.location.href='student.php?mode=form&matnr=".$_POST['matnr']."'\")>\n";
       createFooter();
     }
     break;
   case 'delete':
     if( $_GET['groupname'] ) {
       $matnr_array = $db->findLearngroupsMatnr($_GET['groupname']);
       foreach( $matnr_array as $matnr )
	 $db->deleteCoaching($matnr['matnr'],$_GET['timestamp']);
       print "<body onload=\"window.location.href='student.php?mode=form&groupname=".$_GET['groupname']."'\")>\n";
       createFooter();
     }
     if( $_GET['matnr'] ) {
       $db->deleteCoaching($_GET['matnr'],$_GET['timestamp']);
       print "<body onload=\"window.location.href='student.php?mode=form&matnr=".$_GET['matnr']."'\")>\n";
       createFooter();
     }
     break;
   }
   break;
 case 'student':
   switch( $_GET['mode'] ) {
   case 'new':
     $data = array('matnr'=>$_POST['matnr'],
		   'last_name'=>$_POST['last_name'],
		   'first_name'=>$_POST['first_name'],
		   'email'=>$_POST['email']);
     $db->insertStudent($data);
     print "<body onload=\"window.location.href='import_export.php?mode=import&target=student'\">\n";
     createFooter();
     break;
   case 'delete':
     $db->deleteStudent($_GET['matnr']);
     print "<body onload=\"window.location.href='index.php'\">\n";
     createFooter();
     break;
   }
   break;
 case 'learngroups':
   switch( $_GET['mode'] ) {
   case 'delete':
     $db->deleteLearngroups($_GET['groupname']);
     print "<body onload=\"window.location.href='student.php?mode=form&learngroups=1'\">\n";
     createFooter();
     break;
   case 'edit':
     if( $_POST['deletegroup'] ) {
       $db->deleteLearngroupsEntry(array( 'groupname'=>$_POST['group'], 'matnr'=>$_GET['matnr']));
     } 
     if( $_POST['group_new'] ) {
       $db->insertLearngroups(array( 'groupname'=>$_POST['group_new'], 'matnr'=>$_GET['matnr']));
     }
     if( $_POST['group_option'] != '---' ) {
       $db->insertLearngroups(array( 'groupname'=>$_POST['group_option'], 'matnr'=>$_GET['matnr']));
     }
     print "<body onload=\"window.location.href='student.php?mode=form&learngroups=1'\">\n";
     createFooter();
     break;
   }
 }

?>